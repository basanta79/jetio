import socket

class SocketBarrier():
    _ip = ""
    _port = 0
    _name: str

    def __init__(self, ip: str, port: int, name: str = None):
        self._name = name
        self._ip = ip
        self._port = port

    def __del__(self):
        print(str(id(self)) + " deleted.")

    def sendToJetIO(self, msg: str) -> str:
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.settimeout(3)
            s.connect((self._ip, self._port))
            b = []
            for xx in range(0, len(msg),2):
                b.append((int('0x' + msg[xx] + msg[xx+1], base=0)))
            s.send(bytes(b))
            data = s.recv(1024)
            # s.close()
        except socket.error as e:
            data=e
        finally:
            print('error')
            s.close()
            del s
        return data
    
