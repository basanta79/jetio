# DosiJetIo

dosiJetio is a package to set or reset bits on a jet I/O module.
It was created in python to use inside a django application

## Getting Started

Clone this repo. 
First development was done with Python 3.7.2
No extra dependecies are needed.

### Use


```
# Init comms and select bit to use as bit for the barrier
barr = jetioBarrier('10.0.0.121', 502, 1)
# to set Heigh level
barr._openBarrier_()
# to set Low level
barr._closeBarrier_()
```

### Prerequisites

No prerequisites for installation

### Installing

No installation expirience so far

## Running the tests

No test so far

### Break down into end to end tests

No test so far

### And coding style tests



## Deployment



## Built With

Python 3.7.2

## Contributing

Please read contributing.md for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the tags on this repository.

## Authors

* **Pablo L Basanta** - *Initial work* - 

## License



## Acknowledgments
